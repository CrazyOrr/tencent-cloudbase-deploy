# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.1

- patch: Fix exit status issue

## 0.3.0

- minor: Use CloudBase Framework to deploy

## 0.2.2

- patch: Print debug infos

## 0.2.1

- patch: Run docker as root user

## 0.2.0

- minor: Try to fix access issue

## 0.1.2

- patch: Fix deploy paths issue.

## 0.1.1

- patch: Fix deploy_args error.

## 0.1.0

- minor: Initial release

