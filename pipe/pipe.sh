#!/usr/bin/env bash
#
# Tencent CloudBase deploy pipe
#
# Required files:
#   ./cloudbaserc.json
#
# Required globals:
#   SECRET_ID
#   SECRET_KEY
#
# Optional globals:
#   DEBUG (default: "false")
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
SECRET_ID=${SECRET_ID:?"SECRET_ID variable missing."}
SECRET_KEY=${SECRET_KEY:?"SECRET_KEY variable missing."}
CONFIG_FILE="./cloudbaserc.json"

# Default parameters
DEBUG=${DEBUG:="false"}

#
# Config file check
#
if [ ! -f "$CONFIG_FILE" ]; then
    echo "::error:: Missing cloudbase configuration file"
    exit 1
fi

if [[ "${DEBUG}" == "true" ]]; then
  enable_debug
fi

debug "Login with API key..."
tcb login --apiKeyId "${SECRET_ID}" --apiKey "${SECRET_KEY}"

debug "Deploying..."
tcb framework deploy

if [[ "${status}" -eq 0 ]]; then
  success "Success!"
else
  fail "Error!"
fi
