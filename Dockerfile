FROM node:10-alpine3.10

RUN apk --no-cache add \
    bash=5.0.0-r0 \
    && npm install -g @cloudbase/cli

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

RUN chmod a+x /*.sh

USER root

ENTRYPOINT ["/pipe.sh"]
