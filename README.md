# Bitbucket Pipelines Pipe: Tencent CloudBase deploy

Deploy to Tencent CloudBase (only supports hosting for now).

将 Bitbucket 项目自动部署到腾讯云开发环境（目前支持静态托管功能）。

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: CrazyOrr/tencent-cloudbase-deploy:0.3.1
    variables:
      SECRET_ID: '<string>'
      SECRET_KEY: '<string>'
      # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| SECRET_ID (*)         | Tencent CloudBase API key SecretId. 腾讯云开发 API 密钥 SecretId |
| SECRET_KEY (*)        | Tencent CloudBase API key SecretKey. 腾讯云开发 API 密钥 SecretKey |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

This pipe deploys code and assets from your project directory to your Tencent CloudBase project.

## Prerequisites
- Make sure you add [cloudbaserc.json](https://docs.cloudbase.net/framework/config.html) in the root of your project directory for using [CloudBase Framework](https://github.com/Tencent/cloudbase-framework)
- You are going to need to generate an API authentication key for use in non-interactive environments.
  - To [generate the API authentication key](https://docs.cloudbase.net/cli-v1/login.html#teng-xun-yun-yun-api-mi-yao-shou-quan).
  - Add the generated SecretId and SecretKey as [secured environment variables](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.

## Examples

```yaml
script:
  - pipe: CrazyOrr/tencent-cloudbase-deploy:0.3.1
    variables:
      SECRET_ID: $TCB_SECRET_ID
      SECRET_KEY: $TCB_SECRET_KEY
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by wanglei021212@gmail.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2021 Lei Wang.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
